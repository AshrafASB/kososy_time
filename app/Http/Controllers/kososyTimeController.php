<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\MainPage;
use App\Packages;
use App\Students;
use App\Teacher;
use App\WhoAreWe;
use Illuminate\Http\Request;

class kososyTimeController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "welcome";
    }

    public function index(){
//      MainPage
        $mainPages = MainPage::all()->first();


//      why us is awesome
        $WhyUsAwesome = WhoAreWe::all();

//      Teacher
        $teachers = Teacher::all();

//      Packages
        $packages = Packages::all();


        return view($this->path,compact(['mainPages','WhyUsAwesome','teachers','packages']));

    }//end of index

    public function teacher (){
        $teachers = Teacher::all();
        return view('teacher',compact(['teachers']));

    }//end of teacher


    public function store (Request $request){
//        dd($request->all());

        ContactUs::create($request->all());

        session()->flash('success',__('site.call_us'));
        return view('thanks');
    }//end of store


//    Student Method
    public function studentRequest (){
        $teacher = Teacher::all();
        $package = Packages::all();
        return view('student_request',compact(['teacher','package']));
    }//end of studentRequest


    public function storeStd (Request $request){
//        dd($request->all());

        $data = $request->except(['photo']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            $data['photo'] = $photo;
        }
        Students::create($data);
        return view('joinMessage');
    }//end of store

}
