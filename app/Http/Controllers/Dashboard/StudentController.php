<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.students.";

        //Permissions
        $this->middleware('permission:read_student')->only(['index']);
        $this->middleware('permission:create_student')->only(['create','store']);
        $this->middleware('permission:update_student')->only(['edit','update']);
        $this->middleware('permission:delete_student')->only(['destroy']);

    }

    public function index()
    {
        $students = Students::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('students'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:teachers,email',
        ]);

//        $data = $request->all();
        $data = $request->except(['photo']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            $data['photo'] = $photo;
//            dd($data);
        }
        Students::create($data);
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(Students $student)
    {
        return view($this->path.'show',compact('student'));
    }//end of show

    public function edit(Students $student)
    {
        return view($this->path.'create',compact('student'));
    }//end of edit

    public function update(Request $request, Students $student)
    {
        $request->validate([
//            'email' => 'required|unique:teachers,email',
        ]);

//        $data = $request->all();
        $data = $request->except(['photo']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            Storage::disk('public')->delete($student->photo);
            $data['photo'] = $photo;
        }

        $student->update($data);
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Students $student)
    {
        if ($student->photo){
            Storage::disk('public')->delete($student->photo);
        }
        $student->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
