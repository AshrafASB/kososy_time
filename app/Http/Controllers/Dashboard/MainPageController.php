<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\MainPage;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.mainPages.";

        //Permissions
        $this->middleware('permission:read_mainPages')->only(['index']);
        $this->middleware('permission:create_mainPages')->only(['create','store']);
        $this->middleware('permission:update_mainPages')->only(['edit','update']);
        $this->middleware('permission:delete_mainPages')->only(['destroy']);

    }

    public function index()
    {
        $mainPages = MainPage::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('mainPages'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:main_pages,title',
        ]);
        MainPage::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(MainPage $mainPage)
    {
        return view($this->path.'create',compact('mainPage'));
    }//end of edit

    public function update(Request $request, MainPage $mainPage)
    {
        $request->validate([
            'title' => 'required|unique:main_pages,title,'.$mainPage->id,
        ]);
        $mainPage->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(MainPage $mainPage)
    {
        $mainPage->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
