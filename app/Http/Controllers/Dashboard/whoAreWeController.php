<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\WhoAreWe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class whoAreWeController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.WhoAreWes.";

        //Permissions
        $this->middleware('permission:read_WhoAreWe')->only(['index']);
        $this->middleware('permission:create_WhoAreWe')->only(['create','store']);
        $this->middleware('permission:update_WhoAreWe')->only(['edit','update']);
        $this->middleware('permission:delete_WhoAreWe')->only(['destroy']);

    }

    public function index()
    {
        $WhoAreWes = WhoAreWe::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('WhoAreWes'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:who_are_wes,name',
        ]);

        $data = $request->only(['name','description']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            $data['photo'] = $photo;
        }

        WhoAreWe::create($data);
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function edit(WhoAreWe $WhoAreWe)
    {
        return view($this->path.'create',compact('WhoAreWe'));
    }//end of edit

    public function update(Request $request, WhoAreWe $WhoAreWe)
    {
//        dd($WhoAreWe);
        $request->validate([
            'name' => 'required|unique:who_are_wes,name,'.$WhoAreWe->id,
        ]);

        $data = $request->only(['name','description']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            Storage::disk('public')->delete($WhoAreWe->photo);
            $data['photo'] = $photo;
        }

        $WhoAreWe->update($data);

        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(WhoAreWe $WhoAreWe)
    {
        if ($WhoAreWe->photo){
            Storage::disk('public')->delete($WhoAreWe->photo);
        }
        $WhoAreWe->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
