<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Packages;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.packages.";

        //Permissions
        $this->middleware('permission:read_packages')->only(['index']);
        $this->middleware('permission:create_packages')->only(['create','store']);
        $this->middleware('permission:update_packages')->only(['edit','update']);
        $this->middleware('permission:delete_packages')->only(['destroy']);

    }

    public function index()
    {
        $packages = Packages::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('packages'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:packages,name',
        ]);
        Packages::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Packages $package)
    {
        return view($this->path.'create',compact('package'));
    }//end of edit

    public function update(Request $request, Packages $package)
    {
        $request->validate([
            'name' => 'required:packages,name,'.$package->id,
        ]);
        $package->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Packages $package)
    {
        $package->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
