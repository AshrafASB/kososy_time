<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TeacherController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.teachers.";

        //Permissions
        $this->middleware('permission:read_teacher')->only(['index']);
        $this->middleware('permission:create_teacher')->only(['create','store']);
        $this->middleware('permission:update_teacher')->only(['edit','update']);
        $this->middleware('permission:delete_teacher')->only(['destroy']);

    }

    public function index()
    {
        $teachers = Teacher::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('teachers'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:teachers,email',
        ]);

//        $data = $request->all();
        $data = $request->except(['photo']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            $data['photo'] = $photo;
//            dd($data);
        }
        Teacher::create($data);
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Teacher $teacher)
    {
        return view($this->path.'create',compact('teacher'));
    }//end of edit

    public function update(Request $request, Teacher $teacher)
    {
        $request->validate([
//            'email' => 'required|unique:teachers,email',
        ]);

//        $data = $request->all();
        $data = $request->except(['photo']);

        if ($request->hasFile('photo')){
            $photo = $request->photo->store('images','public');
            Storage::disk('public')->delete($teacher->photo);
            $data['photo'] = $photo;
        }

        $teacher->update($data);
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Teacher $teacher)
    {
        if ($teacher->photo){
            Storage::disk('public')->delete($teacher->photo);
        }
        $teacher->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
