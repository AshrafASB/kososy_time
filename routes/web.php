<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/teacher', function () {
    return view('teacher');
})->name('teacher');
Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');

Route::get('/thanks', function () {
    return view('thanks');
})->name('thanks');


//Route::get('/student_request', function () {
//    return view('student_request');
//})->name('student_request');


Auth::routes();
//al mosafer
Route::get('/home', 'kososyTimeController@index')->name('home');


//Kososy time
Route::get('/', 'kososyTimeController@index')->name('/');
Route::get('/teacher', 'kososyTimeController@teacher')->name('teacher');
Route::post('/store', 'kososyTimeController@store')->name('store');

//Student
Route::get('/student_request', 'kososyTimeController@studentRequest')->name('student_request');
Route::post('/std_request', 'kososyTimeController@storeStd')->name('std_request');

//Route::post('consultation_requests','App\Http\Controllers\Dashboard\Consultation_requestsController@store')->name('consultation_requests');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->where('provider','facebook|google|youtube');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->where('provider','facebook|google|youtube');

