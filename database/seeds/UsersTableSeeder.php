<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name'=>'Ahmad',
            'email'=>'Ahmad_hosam@loay.com',
            'password'=>bcrypt('Ahmad_hosam@loay.com'),
            'email_verified_at'=>Carbon::now(),
//            'type_user'=>'super_admin'
        ]);

        $user->attachRoles(['super_admin']);
    }
}
