@include('headerWelcome')
<body id="page-top" data-spy="scroll" data-target=".side-menu">


<div class="container">

    <!-- Start: Speakers -->
    <div class="row me-row content-ct speaker" id="speakers">
        <h2 class="row-title" id="teacher">اختر مدرسك </h2>
        @foreach( $teachers as $index => $teacher )
            <a href="{{route('student_request')}}">

            <div class="col-md-4 col-sm-6 feature">
                    <img width="100px" height="100px" src="{{asset('storage/'.$teacher->photo)}}" alt="" class="speaker-img">
                    {{--            <img src="{{asset("design/img/speaker-1.png")}}" class="speaker-img">--}}
                    <h3><b>name :</b> {{ isset($teacher->name)?$teacher->name:''  }} </h3>
                    <p><b>Email :</b> {{ isset($teacher->email)?$teacher->email:''  }}</p>
                    <p><b>Phono :</b> {{ isset($teacher->phone)?$teacher->phone:''  }}</p>
                    <p><b>Subject :</b> {{ isset($teacher->subject)?$teacher->subject:''  }}</p>
                    <p><b>Experience year :</b> {{ isset($teacher->experience_year)?$teacher->experience_year:''  }}</p>
                    <p><b>period :</b>  {{ isset($teacher->period)?$teacher->period:''  }}</p>
                    <ul class="speaker-social">
                        <li><a href="{{ isset($teacher->facebook_link)?$teacher->facebook_link:'#'  }}"><span class="ti-facebook"></span></a></li>
                        <li><a href="{{ isset($teacher->twitter_link)?$teacher->twitter_link:'#'  }}"><span class="ti-twitter-alt"></span></a></li>
                        <li><a href="{{ isset($teacher->youtube_link)?$teacher->youtube_link:'#'  }}"><span class="ti-youtube"></span></a></li>
                    </ul>
                </div>
            </a>
        @endforeach

    </div>
    <a class="btn btn-seccess" style="border:skyblue 2px solid; margin-top: 20px; margin-left: 40%; margin-bottom: 10px"  href="{{route('/')}}"><h2> back</h2></a>


    <!-- End: Speakers -->
</div>
@include('footerWelcome')
