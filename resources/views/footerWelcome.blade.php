
<!-- Start: Footer -->
<div class="container-fluid footer">
    <div class="row contact">
        <div class="col-md-6 contact-form">
            @include('dashboard.partials._seession2')
            @include('dashboard.partials._errors')
            <h3 class="content-ct"><span class="ti-email"></span> تواصل معنا وأعطينا رأيك في موقعنا</h3>
            <form class="form-horizontal" data-toggle="validator" role="form" method="post" action="{{route('store')}}">
                @csrf
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Name<sup>*</sup></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="John Doe" required>
                        <div class="help-block with-errors pull-right"></div>
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email<sup>*</sup></label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" name="email" autocomplete="off" placeholder="you@youremail.com" required>
                        <div class="help-block with-errors pull-right"></div>
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">Your Message<sup>*</sup></label>
                    <div class="col-sm-9">
                        <textarea id="message" name="message" class="form-control" rows="3" required></textarea>
                        <div class="help-block with-errors pull-right"></div>
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" id="submit" class="btn btn-yellow pull-right">Send <span class="ti-arrow-right"></span></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4 col-md-offset-1 content-ct">
            <h3><span class="ti-twitter"></span> Message Feed</h3>
            <p>Lorem <a href="#">#Ipsum</a> is a dummy text used as a text filler in designs. This is just a dummy text. via <a href="#">@designerdada</a> </p>
            <hr>
            <p>Lorem Ipsum is a <a href="#">#dummy</a> text used as a text filler in designs. This is just a dummy text. via <a href="#">@designerdada</a> </p>
            <hr>
            <p>Lorem Ipsum is a <a href="#">#dummy</a> text used as a text filler in designs. This is just a dummy text. via <a href="#">@designerdada</a> </p>
        </div>
    </div>
    <div class="row footer-credit">
        <div class="row me-row content-ct speaker" >
            <h2 class="row-title" id="teacher">تواصل معنا عبر السوشيال ميديا</h2>
            <div class="col-md-12 col-lg-12 feature" style="margin-left: 35%" >
                <ul class="speaker-social">
                    <li><a href="{{ setting('facebook_link') }}"><span class="ti-facebook"></span></a></li>
                    <li><a href="{{ setting('twitter_link') }}"><span class="ti-twitter-alt"></span></a></li>
                    <li><a href="{{ setting('linkedin_link') }}"><span class="ti-linkedin"></span></a></li>
                    <li><a href="{{ setting('youtubeChanel_link') }}"><span class="ti-youtube"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-12 col-sm-6" style="text-align: center">
            <p>&copy; 2020, <a href="#"> جميع حقوق الطبع محفوظة لفريق  </a> | خصوصي تايم</p>

        </div>

    </div>
</div>
<!-- End: Footer -->

<script src="{{asset("design/js/jquery.min.js")}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset("design/js/bootstrap.min.js")}}"></script>
<script src="{{asset("design/js/jquery.easing.min.js")}}"></script>
<script src="{{asset("design/js/scrolling-nav.js")}}"></script>
<script src="{{asset("design/js/validator.js")}}"></script>

</body>
</html>
