@include('headerWelcome')
<style>
    .word{
        font-size: 100px !important;
        font-family: "Roboto", "Arial", "Tahoma", "Verdana", "sans-serif";
    }




</style>
<div class="jumbotron text-center">
    <h1 class="word">لقد تم انضمامك بنجاح لعائلة خصوصي تايم</h1>
    <p class="lead"><strong>سيتم التواصل معك عند اكتمال المجموعات</strong> شكرا لك لانضمامك لعائلتنا .</p>
    <hr>
    <p>
        في حال احتجت أي مساعدة تواصل معنا على الواتساب الخاص بنا<br>
        <a href="https://api.whatsapp.com/send?phone={{ setting('whatsUp_link') }}"><img src="https://img.icons8.com/doodle/48/000000/whatsapp.png"/></a>
    </p>
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="{{route('/')}}" role="button">Continue to homepage</a>
    </p>
</div>
@include('footerWelcome')
