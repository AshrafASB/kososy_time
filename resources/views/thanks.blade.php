@include('headerWelcome')
<style>
    .word{
        font-size: 200px !important;
        font-family: "Roboto", "Arial", "Tahoma", "Verdana", "sans-serif";
    }




</style>
<div class="jumbotron text-center">
    <h1 class="word">Thank You!</h1>
    <p class="lead"><strong>We are received your Message</strong> Please wait for us to contact you very soon .</p>
    <hr>
    <p>
        Having trouble? Call us in Whats App<br>
        <a href="https://api.whatsapp.com/send?phone={{ setting('whatsUp_link') }}"><img src="https://img.icons8.com/doodle/48/000000/whatsapp.png"/></a>
    </p>
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="{{route('/')}}" role="button">Continue to homepage</a>
    </p>
</div>
@include('footerWelcome')
