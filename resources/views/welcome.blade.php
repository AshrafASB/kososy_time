@include('headerWelcome')
<body id="page-top" data-spy="scroll" data-target=".side-menu">
<nav class="side-menu">
    <ul>
        <li class="hidden active">
            <a class="page-scroll" href="#page-top"></a>
        </li>
        <li>
            <a href="#home" class="page-scroll">
                <span class="menu-title">Home</span>
                <span class="dot"></span>
            </a>
        </li>
        <li>
            <a href="#speakers" class="page-scroll">
                <span class="menu-title">Teachers</span>
                <span class="dot"></span>
            </a>
        </li>
        <li>
            <a href="#tickets" class="page-scroll">
                <span class="menu-title">Packges</span>
                <span class="dot"></span>
            </a>
        </li>
        <li>
            <a href="#teams" class="page-scroll">
                <span class="menu-title">Teams</span>
                <span class="dot"></span>
            </a>
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <!-- Start: Header -->
    <div class="row hero-header" id="home">
        <div class="col-md-7">

            <!-- <img src="img/meetup-logo.png" class="logo"> -->
            <h1 style="text-align: center">{{ isset($mainPages->title)?$mainPages->title:''  }}</h1>
            <h1 style="text-align: center"> {{ isset($mainPages->description1)?$mainPages->description1:''  }}</h1>
            <p style="text-align: center">---------------------</p>
            <h3 style="text-align: center">{{ isset($mainPages->description2)?$mainPages->description2:''  }}</h3>
            <h4 style="text-align: center">Site Created in : {{ isset($mainPages->day)?$mainPages->day:''  }}<sup>th</sup> {{ isset($mainPages->month)?$mainPages->month:''  }}, {{ isset($mainPages->year)?$mainPages->year:''  }}</h4>
            <a href="#teacher"  class="btn btn-lg btn-red page-scroll">اختر مُدرسك <span class="ti-arrow-right"></span></a>

        </div>
        <div class="col-md-5 hidden-xs">
            <img src="{{asset("design/img/rocket.png")}}" class="rocket animated bounce">
        </div>
    </div>
    <!-- End: Header -->
</div>
<div class="container">
    <!-- Start: Desc -->
    <div class="row me-row content-ct">
        <h2 class="row-title">لماذا خصوصي تايم في غاية الروعة؟</h2>
        @foreach($WhyUsAwesome as $WhyUsAwesomeitme)

            <div class="col-md-4 feature">
                <img width="100px" height="100px" src="{{asset('storage/'.$WhyUsAwesomeitme->photo)}}" alt="">
                <h3> {{ isset($WhyUsAwesomeitme->name)?$WhyUsAwesomeitme->name:''  }}</h3>
                <p>{{ isset($WhyUsAwesomeitme->description)?$WhyUsAwesomeitme->description:''  }}</p>
            </div>
        @endforeach


    </div>
    <!-- End: Desc -->

    <!-- Start: Speakers -->
    <div class="row me-row content-ct speaker" id="speakers">
        <h2 class="row-title" id="teacher">اختر مدرسك </h2>
        @foreach( $teachers as $index => $teacher )
            @if($index  <= 5  )
                <a href="{{route('student_request')}}">
                <div class="col-md-4 col-sm-6 feature">
                    <img width="100px" height="100px" src="{{asset('storage/'.$teacher->photo)}}" alt="" class="speaker-img">
                    {{--            <img src="{{asset("design/img/speaker-1.png")}}" class="speaker-img">--}}
                    <h3><b>name :</b> {{ isset($teacher->name)?$teacher->name:''  }} </h3>
                    <p><b>Email :</b> {{ isset($teacher->email)?$teacher->email:''  }}</p>
                    <p><b>Phono :</b> {{ isset($teacher->phone)?$teacher->phone:''  }}</p>
                    <p><b>Subject :</b> {{ isset($teacher->subject)?$teacher->subject:''  }}</p>
                    <p><b>Experience year :</b> {{ isset($teacher->experience_year)?$teacher->experience_year:''  }}</p>
                    <p><b>period :</b> {{ isset($teacher->period)?$teacher->period:''  }}</p>
                    <ul class="speaker-social">
                        <li><a href="{{ isset($teacher->facebook_link)?$teacher->facebook_link:'#'  }}"><span class="ti-facebook"></span></a></li>
                        <li><a href="{{ isset($teacher->twitter_link)?$teacher->twitter_link:'#'  }}"><span class="ti-twitter-alt"></span></a></li>
                        <li><a href="{{ isset($teacher->youtube_link)?$teacher->youtube_link:'#'  }}"><span class="ti-youtube"></span></a></li>
                    </ul>
                </div>
                </a>
            @endif
        @endforeach

    </div>
    <a class="btn btn-seccess" style="border:skyblue 2px solid; margin-top: 20px; margin-left: 40%; margin-bottom: 10px"  href="{{route('teacher')}}"><h2> Show more</h2></a>
    <!-- End: Speakers -->
</div>

<!-- Start: Tickets -->
<div class="container-fluid tickets" id="tickets">
    <div class="row me-row content-ct">
        <h2 class="row-title">باقات الحجز</h2>

        @foreach( $packages as $package )
            <div class="col-md-4 col-sm-6">
                <h3>{{ isset($package->name)?$package->name:''  }}</h3>
                <p class="price">{{ isset($package->currency)?$package->currency:''  }}  {{ isset($package->prise)?$package->prise:''  }}</p>
                <p>{{ isset($package->description)?$package->description:''  }}</p>
                <a href="{{route('student_request')}}" class="btn btn-lg btn-red">احجز الأن</a>
            </div>
        @endforeach


    </div>
</div>
<!-- End: Tickets -->

<!-- Start: our Team -->
<div class="row me-row content-ct speaker" id="teams">
    <h2 class="row-title" id="teacher">فريق عمل الموقع</h2>
    <div class="col-md-4 col-sm-6 feature">
        <img style="max-height: 50%" src="{{asset("design/img/ahmad.jpeg")}}" class="speaker-img">
        <h3>احمد حسين عودة</h3>
        <p>طالب في جامعة القدس المفتوحة</p>
        <p>رقم الجوال : 0597185684</p>
        <ul class="speaker-social">
            <li><a href="#"><span class="ti-facebook"></span></a></li>
            <li><a href="#"><span class="ti-twitter-alt"></span></a></li>
            <li><a href="#"><span class="ti-linkedin"></span></a></li>
        </ul>
    </div>
     <div class="col-md-4 col-sm-6 feature">
        <img style="max-height: 50%" src="{{asset("design/img/hosam.jpeg")}}" class="speaker-img">
        <h3>حسام ابراهيم عليان</h3>
         <p>طالب في جامعة القدس المفتوحة</p>
         <p>رقم الجوال : 0592715584</p>

         <ul class="speaker-social">
            <li><a href="#"><span class="ti-facebook"></span></a></li>
            <li><a href="#"><span class="ti-twitter-alt"></span></a></li>
            <li><a href="#"><span class="ti-linkedin"></span></a></li>
        </ul>
    </div>
     <div class="col-md-4 col-sm-6 feature">
        <img style="height: auto" src="{{asset("design/img/loay.jpeg")}}" class="speaker-img">
        <h3>لؤي مهدي ابو عمشة</h3>
         <p>طالب في جامعة القدس المفتوحة</p>
         <p>رقم الجوال : 0599096913</p>

         <ul class="speaker-social">
            <li><a href="#"><span class="ti-facebook"></span></a></li>
            <li><a href="#"><span class="ti-twitter-alt"></span></a></li>
            <li><a href="#"><span class="ti-linkedin"></span></a></li>
        </ul>
    </div>

</div>
<!-- End:  our Team -->


@include('footerWelcome')
