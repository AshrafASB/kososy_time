@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($mainPage)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Main Page')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Main Page') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.mainPages.index')}}">{{__('site.Main Page')}}</a></li>
            @if(isset($mainPage))
                <li class="breadcrumb-item">{{__('site.Update Main Page')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Main Page')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($mainPage)?route('dashboard.mainPages.update',$mainPage->id):route('dashboard.mainPages.store')}}" method="post">
                 @csrf
                 @if(isset($mainPage))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')
                 <div class="form-group">
                     <label>{{__('site.title')}} :</label>
{{--                     <textarea title="description" cols="30" rows="10"  class="form-control">{{isset($mainPage)?$mainPage->description:""}}</textarea>--}}
                         <input type="text" name="title" class="form-control" value="{{isset($mainPage)?$mainPage->title:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.description1')}} :</label>
                     <textarea name="description1" cols="30" rows="10"  class="form-control">{{isset($mainPage)?$mainPage->description1:""}}</textarea>
{{--                     <input type="text" name="description1" class="form-control" value="{{isset($mainPage)?$mainPage->description1:""}}">--}}
                 </div>
                 <div class="form-group">
                     <label>{{__('site.description2')}} :</label>
                     <textarea name="description2" cols="30" rows="10"  class="form-control">{{isset($mainPage)?$mainPage->description2:""}}</textarea>
{{--                         <input type="text" name="description2" class="form-control" value="{{isset($mainPage)?$mainPage->description2:""}}">--}}
                 </div>

                 <div class="form-group">
                     <label>{{__('site.day')}} :</label>
{{--                     <textarea name="day" cols="30" rows="10"  class="form-control">{{isset($mainPage)?$mainPage->day:""}}</textarea>--}}
                     <input type="text" name="day" class="form-control" value="{{isset($mainPage)?$mainPage->day:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.month')}} :</label>
{{--                     <textarea name="month" cols="30" rows="10"  class="form-control">{{isset($mainPage)?$mainPage->month:""}}</textarea>--}}
                     <input type="text" name="month" class="form-control" value="{{isset($mainPage)?$mainPage->month:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.year')}} :</label>
{{--                     <textarea name="year" cols="30" rows="10"  class="form-control">{{isset($mainPage)?$mainPage->year:""}}</textarea>--}}
                     <input type="text" name="year" class="form-control" value="{{isset($mainPage)?$mainPage->year:""}}">
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($mainPage) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
