@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($teacher)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Teacher')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Teacher') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.teachers.index')}}">{{__('site.Teacher')}}</a></li>
            @if(isset($teacher))
                <li class="breadcrumb-item">{{__('site.Update Teacher')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Teacher')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($teacher)?route('dashboard.teachers.update',$teacher->id):route('dashboard.teachers.store')}}" method="post" enctype="multipart/form-data">
                 @csrf
                 @if(isset($teacher))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.name')}} :</label>
{{--                     <textarea name="description" cols="30" rows="10"  class="form-control">{{isset($teacher)?$teacher->description:""}}</textarea>--}}
                     <input type="text" name="name" class="form-control" value="{{isset($teacher)?$teacher->name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.email')}} :</label>
                     <input type="text" name="email" class="form-control" value="{{isset($teacher)?$teacher->email:""}}" autocomplete="off">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.Phone')}} :</label>
                     <input type="text" name="phone" class="form-control" value="{{isset($teacher)?$teacher->phone:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.subject')}} :</label>
                     <input type="text" name="subject" class="form-control" value="{{isset($teacher)?$teacher->subject:""}}" autocomplete="off">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.Experience Year')}} :</label>
                     <input type="number" name="experience_year" class="form-control" value="{{isset($teacher)?$teacher->experience_year:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.Photo')}} :</label><br>
                     @isset($teacher)
                         <img width="100px" height="100px" src="{{asset('storage/'.$teacher->photo)}}" alt="">
                         <input type="file" name="photo" class="form-control" >
                     @else
                         <input type="file" name="photo" class="form-control" >
                     @endisset
                 </div>

                 <div class="form-group">
                     <label>{{__('site.Summary')}} :</label>
                      <textarea name="description" cols="30" rows="10"  class="form-control">{{isset($teacher)?$teacher->description:""}}</textarea>
{{--                     <input type="text" name="name" class="form-control" value="{{isset($teacher)?$teacher->name:""}}">--}}
                 </div>

                 <div class="form-group">
                     <label>{{__('site.Period')}} :</label>
                     <input type="text" name="period" class="form-control" value="{{isset($teacher)?$teacher->period:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.Facebook links')}} :</label>
                     <input type="text" name="facebook_link" class="form-control" value="{{isset($teacher)?$teacher->facebook_link:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.twitter links')}} :</label>
                     <input type="text" name="twitter_link" class="form-control" value="{{isset($teacher)?$teacher->twitter_link:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.youtube links')}} :</label>
                     <input type="text" name="youtube_link" class="form-control" value="{{isset($teacher)?$teacher->youtube_link:""}}">
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($teacher) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
