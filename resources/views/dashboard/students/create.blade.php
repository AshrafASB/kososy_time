@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($package)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Packages')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Packages') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.packages.index')}}">{{__('site.Packages')}}</a></li>
            @if(isset($package))
                <li class="breadcrumb-item">{{__('site.Update Packages')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Packages')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($package)?route('dashboard.packages.update',$package->id):route('dashboard.packages.store')}}" method="post">
                 @csrf
                 @if(isset($package))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')
                 <div class="form-group">
                     <label>{{__('site.name')}} :</label>
{{--                     <textarea name="description" cols="30" rows="10"  class="form-control">{{isset($package)?$package->description:""}}</textarea>--}}
                         <input type="text" name="name" class="form-control" value="{{isset($package)?$package->name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.prise')}} :</label>
{{--                     <textarea name="prise" cols="30" rows="10"  class="form-control">{{isset($package)?$package->prise:""}}</textarea>--}}
                         <input type="text" name="prise" class="form-control" value="{{isset($package)?$package->prise:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.currency')}} :</label>
                     {{--                     <textarea name="currency" cols="30" rows="10"  class="form-control">{{isset($package)?$package->currency:""}}</textarea>--}}
                     <input type="text" name="currency" class="form-control" value="{{isset($package)?$package->currency:""}}">
                 </div>
                 <div class="form-group">
                     <labelالمستوى؟ الاكاديمي :</label>
                     {{--                     <textarea name="currency" cols="30" rows="10"  class="form-control">{{isset($package)?$package->currency:""}}</textarea>--}}
                     <input type="text" name="currency" class="form-control" value="{{isset($package)?$package->currency:""}}">
                 </div>ؤ

                 <div class="form-group">
                     <label>{{__('site.description')}} :</label>
{{--                     <textarea name="description" cols="30" rows="10"  class="form-control">{{isset($package)?$package->description:""}}</textarea>--}}
                     <input type="text" name="description" class="form-control" value="{{isset($package)?$package->description:""}}">
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($package) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
