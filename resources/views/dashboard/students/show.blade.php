@extends('layouts.dashboard.app')

@section('content')
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 400px;
            margin: auto;
            text-align: center;
        }

        .title1 {
            color: grey;
            font-size: 18px;
        }

        .button11 {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #0B90C4;
            text-align: center;
            cursor: pointer;
            width: 100%;
            font-size: 18px;
        }

        a1 {
            text-decoration: none;
            font-size: 22px;
            color: #0a6ebd;
        }

        .button11:hover, a1:hover {
            opacity: 0.7;
        }
        .tex{
            text-align: left;
            margin-left: 20px;
            margin-right: 20px;
        }

    </style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.Student')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.students.index')}}"> {{__('site.Student')}}</a></li>
            <li class="breadcrumb-item"> {{__('site.Show')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">

                <!-- Add icon library -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

                <div class="card">
                    <img width="100px" height="300px" src="{{asset('storage/'.$student->photo)}}" alt="John" style="width:100%">
                    <h1>{{$student->name}}</h1>
                    <p class="title1">{{$student->email}}</p>
                    <p class="tex"><b>Phone 1 : </b>{{$student->phone1}}</p>
                    <p class="tex"><b>Phone 2 : </b>{{$student->phone2}}</p>
                    <p class="tex"><b>age : </b>{{$student->age}}</p>
                    <div class="form-group tex">
                        <label>{{__('site.notes')}} :</label>
                        <textarea name="description" cols="30" rows="10"  disabled class="form-control">{{isset($student)?$student->note:""}}</textarea>
                    </div>
                    <p><b>Teacher Team : </b>{{$student->teacher->name}}</p>
                    <p><b>package Type : </b>{{$student->package->name}}</p>

                    <p><a class="button11" href="{{route('dashboard.students.index')}}">Back</a></p>
                </div>


{{--                        @foreach($students as $index=>$student)--}}
{{--                            <tr>--}}
{{--                                <td>{{++$index}}</td>--}}
{{--                                <td>{{$student->name}}</td>--}}
{{--                                <td>{{$student->email}}</td>--}}
{{--                                <td>{{$student->phone}}</td>--}}
{{--                                <td>{{$student->subject}}</td>--}}
{{--                                <td>{{$student->experience_year}}</td>--}}
{{--                                <td><img width="100px" height="100px" src="{{asset('storage/'.$student->photo)}}" alt=""></td>--}}
{{--                                <td>{{$student->period}}</td>--}}
{{--                                <td> {{\Illuminate\Support\Str::limit($student->description, 100)}} </td>--}}
{{--                                <td>--}}
{{--                                    --}}{{--Edit buttom--}}
{{--                                    @if(auth()->user()->hasPermission('update_teacher'))--}}
{{--                                        <a href="{{route('dashboard.teachers.edit', $student->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">Edit</i></a>--}}
{{--                                    @else--}}
{{--                                        <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>--}}
{{--                                    @endif--}}

{{--                                    --}}{{--Delete buttom--}}
{{--                                    @if(auth()->user()->hasPermission('delete_teacher'))--}}
{{--                                        <form action="{{route('dashboard.teachers.destroy', $student->id)}}" method="post" style="display: inline-block">--}}
{{--                                            @csrf--}}
{{--                                            @method('delete')--}}
{{--                                            <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>--}}
{{--                                        </form>--}}
{{--                                    @else--}}
{{--                                        <a href="#" disabled="" class="btn btn-danger btn-sm"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>--}}
{{--                                    @endif--}}

{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}


            </div>
        </div>
    </div>{{--end-of-tile mb-4--}}


@endsection
